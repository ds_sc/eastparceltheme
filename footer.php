<?php

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
		<div class="site-info">
			<!-- #link 
			<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'eastparceltheme' ) ); ?>">
				<?php
				/* translators: %s: CMS name, i.e. WordPress. */
				printf( esc_html__( 'Proudly powered by %s', 'eastparceltheme' ), 'WordPress' );
				?>
			</a>
			
			<span class="sep"> | </span>
			-->
				<?php
				/* translators: 1: Theme name, 2: Theme author. */
				printf( esc_html__( 'Theme: %1$s by %2$s.', 'eastparceltheme' ), 'eastparceltheme', 'Starky\'s Club' );
				?>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
